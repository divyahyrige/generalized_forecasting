from app import app
from flask import request, jsonify, Response
from pandas.io.json import json_normalize

from app.Model import SArima, PmdArima
from app.Orderselect import gridSearch
from app.preprocessing import preprocessing, perform_adf_test, split_train_test, select_train_attribute, \
    select_inputdata, initial_Setup, split_train_test_standard
import pandas as pd
from sklearn.metrics import mean_squared_error,mean_absolute_error
import pickle

from statsmodels.tsa.arima.model import ARIMAResults


@app.route("/post_data", methods=['POST'])
def post():
    """
     :param : content which has data frame values, relevant attribute, target and date column
     :return: time series perediction evaluation with mean square error, model will be created and saved
     """

    # All these below information we will get it from admin console
    content = request.get_json()
    # Call initial set up function to fetch the requied information for the forecasting

    Relevant_att = content["Attributes"]
    Model = content["Model"]
    Date1_col = content['Date_Column']
    target = content['Target_Column']
    Data,potential_col  = initial_Setup(content)

    # Call preprocessing method
    df_p = preprocessing(Data, Relevant_att, Date1_col)


    for i in potential_col:
        if i not in df_p.columns:
            potential_col.remove(i)

    P_VALUE = perform_adf_test(df_p[target])
    if(P_VALUE<0.05):
        pass
    else:
        print("The data is not stationary going for transformation")

     # select order for ARIMA using Grid search
    AIC,regression_param_list,Seasonal_param_list =  gridSearch(df_p[:100],Date1_col,target,5,  potential_col)
    # print(AIC,regression_param_list,Seasonal_param_list)

    # If we need to select data between particular timeframe use below method
    # df_new = select_inputdata(df_p,start_date,end_date)

    # # Test data selection beased on the size
    # train, test = split_train_test(df_new, pd.to_datetime("2020-10-01"))


    # Train and test data selection beased on the standard measures
    train, test = split_train_test_standard(df_p)

    train_y = select_train_attribute(train, target)
    regressors  = [x for x in Relevant_att if x not in [Date1_col,target]]

    train = train[potential_col]
    test = test[potential_col]

    # train_new = train.drop(Date1_col, axis=1)
    # test1 = test.drop("DATE", axis=1)
    # test_new = test
    if(Model == 'PmdArima'):
        forecast = PmdArima(train_y, train)
    elif(Model == 'Arima'):
        results = SArima(train, train_y)
    else:
        pass

    predictsss=results.forecast(len(test),exog=test).to_frame()

    # save the model for future predictions, id instead of name
    with open('arima_model.pkl', 'wb') as handle:
        pickle.dump(results, handle, protocol=2)

    predictsss=predictsss[predictsss["predicted_mean"]>0]
    error = mean_squared_error(test[target ],predictsss)

    # idx = pd.date_range(test_new["DATE"].iloc[0], test_new["DATE"].iloc[-1])
    # predictsss=predictsss.reindex(idx,fill_value=0)
    print(predictsss)
    results = "Model build successfully with mean square error: "+str(error)



    return results


@app.route("/get_prediction", methods=['POST'])
def get():
    """
     :param : content which has exog values : filename_exog_param.json present in input folder
     :return: time series perediction
     """
    print("Prediction Request Received *********** ")
    content = request.get_json()
    Date1_col = content['Date_Column']
    target = content['Target_Column']
    Data = content["File"]
    column_names = Data.pop(0)
    to_forecast = pd.DataFrame(Data, columns=column_names)
    exog_p =  [x for x in to_forecast.columns if x not in [Date1_col]]
    # print(to_forecast['AMOUNT'])
    to_forecast = to_forecast[exog_p]
    model= ARIMAResults.load('arima_model.pkl')
    print(model.summary())
    to_forecast[exog_p] = pd.to_numeric(to_forecast[exog_p].stack(), errors='coerce').unstack()
    to_forecast = to_forecast.dropna(axis=1)
    predictsss=model.forecast(len(to_forecast),exog=to_forecast).to_frame()

    predictsss["DATE"]= predictsss.index.strftime('%Y-%m-%d')

    result = predictsss.to_json(orient = 'records')

    return result
