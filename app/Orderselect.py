import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from pandas.plotting import register_matplotlib_converters
from statsmodels.graphics.tsaplots import plot_acf, plot_pacf
register_matplotlib_converters()
from statsmodels.tsa.stattools import adfuller,kpss
from datetime import datetime
from datetime import timedelta
from statsmodels.tsa.arima_model import ARMA,ARIMA
register_matplotlib_converters()
from statsmodels.tsa.stattools import acf, pacf,acovf,pacf_yw,pacf_ols
register_matplotlib_converters()
from time import time
import statsmodels.api as sm
from statsmodels.tsa.statespace.sarimax import SARIMAX
import pmdarima as pm
from statsmodels.tsa.seasonal import seasonal_decompose
from sklearn.metrics import mean_squared_error,mean_absolute_error
from pmdarima import auto_arima
from statsmodels.tools.eval_measures import rmse
import itertools
import warnings
import random
random.seed(0)
warnings.filterwarnings("ignore") # specify to ignore warning messages

def gridSearch(df,date_col,label_col,seasonal_freq,exog=None):
    #Define the p, d and q parameters to take any value
    p = range(0, 5)
    d = range(0, 2)
    q = range(0, 2)

    # Generate all different combinations of p, q and q triplets
    pdq = list(itertools.product(p, d, q))

    # Generate all different combinations of seasonal p, q and q triplets
    seasonal_pdq = [(x[0], x[1], x[2], seasonal_freq) for x in list(itertools.product(p, d, q))]
    df.set_index(df[date_col],inplace=True)
    df.drop([date_col],axis=1,inplace=True)


    # if exog is none parameters wth exogneous varaibles are found
    if(exog==None):

        AIC=None
        for param in pdq:
            for param_seasonal in seasonal_pdq:
                try:
                    mod = sm.tsa.statespace.SARIMAX(endog=df[label_col],
                                                    order=param,
                                                    seasonal_order=param_seasonal,
                                                    enforce_stationarity=False,
                                                    enforce_invertibility=False)

                    results = mod.fit()
                    if(AIC is None or AIC>results.aic):
                        pdq_list=[]
                        PDQ_list=[]
                        AIC=results.aic
                        pdq_list.append(param[0])
                        pdq_list.append(param[1])
                        pdq_list.append(param[2])

                        PDQ_list.append(param_seasonal[0])
                        PDQ_list.append(param_seasonal[1])
                        PDQ_list.append(param_seasonal[2])
                        PDQ_list.append(param_seasonal[3])
                except Exception as e:
                    print("Exception occurred",e)
                    continue

    else:
        AIC=None
        for param in pdq:
            for param_seasonal in seasonal_pdq:
                try:
                    mod = sm.tsa.statespace.SARIMAX(endog=df[label_col],exog=df[exog],
                                                    order=param,
                                                    seasonal_order=param_seasonal,
                                                    enforce_stationarity=False,
                                                    enforce_invertibility=False)

                    results = mod.fit()
                    if(AIC is None or AIC>results.aic):
                        pdq_list=[]
                        PDQ_list=[]
                        AIC=results.aic
                        pdq_list.append(param[0])
                        pdq_list.append(param[1])
                        pdq_list.append(param[2])

                        PDQ_list.append(param_seasonal[0])
                        PDQ_list.append(param_seasonal[1])
                        PDQ_list.append(param_seasonal[2])
                        PDQ_list.append(param_seasonal[3])
                except Exception as e:
                    print("Exception occurred",e)
                    continue
    return AIC,pdq_list,PDQ_list


