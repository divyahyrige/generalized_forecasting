import pandas as pd
import matplotlib.pyplot as plt
from statsmodels.tsa.arima.model import ARIMA
import pmdarima as pm
from pmdarima.model_selection import train_test_split
import numpy as np
from sktime.forecasting.arima import AutoARIMA
import itertools
import statsmodels.api as sm
import warnings
from sklearn.metrics import mean_squared_error
import joblib

from statsmodels.tsa.arima.model import ARIMAResults

from app.Orderselect import gridSearch
from app.preprocessing import perform_adf_test,test_data

def PmdArima(train_y,test):
    """
     :param train_y: dataframe which is used for training our model
     :param test: the dataframe which is used for testign predictions
     :return: forecast by PmdArima model
     """
    model = pm.auto_arima(train_y["AMOUNT"],seasonal=True,m=5,start_p=0, start_q=0,max_p=4, max_q=4)
    #seasonal True, D=1 enforce seasonality, n_fits=50  # n_jobs=3,stepwise=True
    # make your forecasts
    forecast = model.predict(test.shape[0])
    return forecast

def SArima(train,train_y):
    """
     :param train_new: dataframe which have all the training data
     :param train_y: the dataframe which has only timestamp and target column
     :return: forecast model
     """
    train.set_index(train_y.index,inplace=True)
    # print(train.columns)
    # AIC,regression_param_list,Seasonal_param_list=gridSearch(train_new[:100],"DATE","AMOUNT",5,exog_param)
    mod = sm.tsa.statespace.SARIMAX(endog=train_y["AMOUNT"].astype(float),exog=train,
                                    order=(2,1,2),
                                    seasonal_order=(4,0,1,4),
                                enforce_stationarity=False,
                                enforce_invertibility=False)
    results = mod.fit()
    return results