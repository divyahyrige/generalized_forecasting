import pandas as pd
import numpy as np
import statsmodels.api as sm
import warnings
warnings.filterwarnings('ignore')
from sklearn.preprocessing import StandardScaler
from statsmodels.tsa.stattools import adfuller



def initial_Setup(content):
    """
   :param content: JSON data which needs to converted as dataframe
   :return: python dataframe and list containing required information
   """
    Relevant_att = content["Attributes"]
    Date1_col = content['Date_Column']
    Data = content["File"]
    column_names = Data.pop(0)
    df = pd.DataFrame(Data, columns=column_names)
    potential_col  = [x for x in column_names if x not in [Date1_col]]
    df[potential_col] = pd.to_numeric(df[potential_col].stack(), errors='coerce').unstack()
    return df,potential_col



# Fucntion to remove null vlaue, change date format and perform one-hot encoding
def preprocessing(df,revelavant_col,date_col):
    """
    :param df: dataframe which needs the preprocessing of data
    :param relavant_col, columns which are relavant for time series analysis
    :param date_col, timestamp column in the dataframe
    :return: dataframe with no None values, encoded and indexed with date and removed unwanted columns
    """
    def drop_null(df):
        """
        :param df: dataframe which can have None values
        :return: dataframe which doesn't have None Vlaues
        """
        if not df.isnull().values.any():
            pass
        else:
            df = df.dropna(axis=1)
        return df

    def date_encode(df,date_col):
        """
          :param df: dataframe which has date columns
          :date_col, date column
          :return: dataframe with date fetures extracted
          """
        df[date_col]= pd.to_datetime(df[date_col])
        df[date_col] = pd.to_datetime(df[date_col], infer_datetime_format=True)
        # df[col+'_year'] = df[col].apply(lambda x: x.year)
        # df[col+'_month'] = df[col].apply(lambda x: x.month)
        # df[col+'_day'] = df[col].apply(lambda x: x.day)
        # df['Weekday'] = df[col].apply(lambda x: x.weekday())
        df.set_index(df[date_col],inplace=True)
        return df


    def one_hotencoding(df,col):
        """
        :param df: dataframe which has categorical column and needs one hot encoding
        :col, column that needs one hot endcoding
        :return: dataframe
        """
        weekdays =pd.get_dummies(df[col],prefix=col)
        df = pd.concat([df,weekdays],axis=1)
        return df


    def scale(df):
        """
        :param df: dataframe which needs the standard scaling
        :return: transformed dataframe
        """
        scaler = StandardScaler()
        scaler.fit(df)
        x_train = pd.DataFrame(scaler.transform(df))
        return x_train

    def filter_data(df,col):
        """
        :param df: dataframe which needs some filtering
        :return: dataframe with some data filtering, here removal of weekend
        """
        df=df[pd.to_numeric(df[col])<6]
        return df

    def filter_col(df,revelavant_col):
        df1 =df.copy()
        col = df1.columns
        for c in col:
            # if c not in revelavant_col and c not in potential_col:
            if c not in revelavant_col:
                df1 = df1.drop(c,axis=1)
        return  df

    # drop null values from dataframe
    df = drop_null(df)
    # Remove unneccesory columns for forecasting
    df = filter_col(df,revelavant_col)
    #Extract date features
    df = date_encode(df,date_col)

    # drop unnecessary data
    # df = filter_data(df,'DayofWeek')
    return df

def perform_adf_test(series):
    """
     :param series: series of data
     :return: P-values, which tells the stationarrity of data
     """
    result = adfuller(series)
    print('ADF Statistic: %f' % result[0])
    print('p-value: %f' % result[1])
    return result[1]

def split_train_test(df,date):
    """
    :param df: dataframe which needs to split into train and test
    :return: two dataset train and test
    """
    train = df[df.index<date]
    test =  df[df.index>=date]
    return train ,test

def split_train_test_standard(df):
    df= df.copy()
    train_set, test_set= np.split(df, [int(.9 *len(df))])
    # try:
    #     if(len(test_set)> pred_len):
    #         pass
    # except:
    #     raise("Test length should be atleast equal to prediction length")
    return  train_set, test_set


def select_inputdata(df,startdate,enddate):
    """
     :param df: Original dataframe
     :param startdate, date from which we are considering time series data
     :param enddate, last record in the time series data used for analysis
     :return: dataframe which can be used for time series analysis
     """
    df_new = df.loc[(df.DATE > startdate)]
    df_new = df_new.loc[(df_new.DATE <enddate)]
    return df_new
    #
    # train = df[df.index<date]
    # test =  df[df.index>=date]
    # return

def select_train_attribute(df,col):
    """
     :param df: dataframe which needs some filtering
     :param col: list of columns ww need for training
     :return: dataframe which can be used for time series analysis
     """
    df = df[[col]]
    return df


def transform(df,types):
    """
     :param df: dataframe which needs some transformation
     :return: transformed dataframe
     """
    if types == "firstorder":
        # print("can be handled with arima I component")
            pass
    elif types == "log":
            pass
    elif types == "exp":
            pass
    #     first order transformation
    #     log or exp transformation

    return df

def test_data(data):
    """
     :param data: dataframe which needs some filtering

     :return: dataframe with the filtered values, which can be used to test time series predictions
     """
    test= data[data["DATE"]>= data["STARTDATE"].iloc[0]]

    test=test[["TUESDAY","WEDNESDAY","THURSDAY","FRIDAY", "HOLIDAY", "HOLIDAYWEEK", "DAYSOFHOLIDAY",
               "LONG2B", "LONG1B", "LONG1A", "LONG2A", "SHORT1B", "SHORT1A" , "PROMO" ,"PROMOBIG1B",
               "PROMOBIG1A","PROMOBIG2A"]]
    return test




# revelavant_col= ['DATE','Value']
# # Column needs scaling
# date_col = 'DATE'
# target_col ='Value'
# df = pd.read_csv("./Electric_Production.csv")



# train ,test= preprocessing(df,revelavant_col,date_col)
#
# # Prepare data for analysis with target feature
# y_train=train[[date_col,target_col]]
# y_train = y_train.set_index([date_col])
#
# # train1 =train.copy()
# # train1 =train.dropna(inplace=True)
#
# p_values= perform_adf_test(y_train)
# if(p_values>0.5):
#     print("Data is Non-stationary")
# else:
#     print("Data is stationary")
# print(y_train.head())

# Funciton to test the stationary data and to make data stationary
# def stest(x):
#     result = adfuller(x)
#     if(result[1] >0.05):
#         a=[]
#         a.append(x["Value"].iloc[0])
#         for i in range(len(train)-1):
#             z=x["Value"].iloc[i+1]-x["Value"].iloc[i]
#             a.append(z)
#         return stest(a)
#     else:
#         return result[1]

# next steps removing the trend, seasonality before fitting our model.