from app import app

app.config['ENV']='production'
app.config['DEBUG']=False
app.config['TESTING']=False

if __name__=="__main__":
	app.run(host="0.0.0.0",port=8000,debug=True) #for local remove host will run on =127.0.0.1